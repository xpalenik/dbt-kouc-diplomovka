package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_list_captured_emotions.*
import kotlinx.android.synthetic.main.fragment_list_captured_emotions.view.*
import kotlinx.coroutines.InternalCoroutinesApi
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.viewmodel.CaptureEmotionAnswerViewModel

class ListCapturedEmotionsFragment : Fragment() {

    @InternalCoroutinesApi
    private lateinit var captureEmotionAnswerViewModel : CaptureEmotionAnswerViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_captured_emotions, container, false)
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Recyclerview
        val adapter = ListAdapter()
        val recyclerView = view.recyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // CaptureEmotionAnswerViewModel
        captureEmotionAnswerViewModel = ViewModelProvider(this).get(CaptureEmotionAnswerViewModel::class.java)
        captureEmotionAnswerViewModel.readAllData.observe(viewLifecycleOwner,
            { captureEmotionAnswers ->
                adapter.setData(captureEmotionAnswers)
            })

        floatingActionButton.setOnClickListener {
            val action = ListCapturedEmotionsFragmentDirections.actionCaptureEmotionFragment1()
            Navigation.findNavController(view).navigate(action)
        }
    }

}