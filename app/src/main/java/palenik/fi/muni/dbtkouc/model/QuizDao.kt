package palenik.fi.muni.dbtkouc.model

import androidx.room.Dao
import androidx.room.Query

@Dao
interface QuizDao {

    @Query("SELECT * FROM quiz_tasks")
    suspend fun getAllQuizTasks(): List<QuizTask>

    @Query("SELECT * FROM quiz_tasks WHERE uuid = :quizTaskId")
    suspend fun getQuizTask(quizTaskId: Int): QuizTask

    @Query("DELETE FROM quiz_tasks")
    suspend fun deleteAllQuizTasks()

    @Query("DELETE FROM quiz_tasks WHERE uuid = :quizTaskId")
    suspend fun deleteQuizTask(quizTaskId: Int)

    @Query("SELECT COUNT(task) FROM quiz_tasks")
    suspend fun getRowCount(): Int
}

