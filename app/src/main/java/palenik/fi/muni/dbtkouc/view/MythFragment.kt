package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_myth.*
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.databinding.FragmentMythBinding
import palenik.fi.muni.dbtkouc.viewmodel.MythViewModel


class MythFragment : Fragment() {

    private lateinit var viewModel: MythViewModel
    private lateinit var dataBinding: FragmentMythBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_myth, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this)[MythViewModel::class.java]
        viewModel.fetchFromDatabase()

        buttonNextMyth.setOnClickListener {
            mythEvaluation.visibility = View.GONE
            buttonIsThisMythTruthful.visibility = View.VISIBLE

            viewModel.mythsIterator.let {
                if (it.hasNext()) {
                    val myth = it.next()
                    mythText.text = myth.myth
                    mythEvaluation.text = myth.evaluation
                } else {
                    val action = MythFragmentDirections.actionMenuFragment()
                    Navigation.findNavController(view).navigate(action)
                }
            }
        }

        buttonIsThisMythTruthful.setOnClickListener {
            val fadeInAnimation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.display_myth_fade_in_anim)
            mythEvaluation.startAnimation(fadeInAnimation)
            mythEvaluation.visibility = View.VISIBLE
            buttonIsThisMythTruthful.visibility = View.GONE
        }
    }
}