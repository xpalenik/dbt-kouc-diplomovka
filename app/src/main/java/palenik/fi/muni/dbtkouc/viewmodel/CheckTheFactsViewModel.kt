package palenik.fi.muni.dbtkouc.viewmodel

import android.app.Application
import palenik.fi.muni.dbtkouc.model.CheckTheFactsQuestion

class CheckTheFactsViewModel(application: Application) : BaseViewModel(application) {

    private val questions = arrayListOf(
        CheckTheFactsQuestion(1, "Akú emóciu chceš zmeniť?", "Je náročné pracovať na svojej emócií ak ju nevieme správne pomenovať a popísať. Aby sa nám to podarilo, musíme venovať pozornosť našim myšlienkam, emóciám, telesným pocitom, nutkaniam, rečovým prejavom aj správaniu."),
        CheckTheFactsQuestion(2, "Čo vyvolalo tvoju emočnú reakciu?", "V tomto kroku budeme opisovať fakty, ktoré dokážeme vnímať priamo našimi zmyslami. Budeme si overovať, či naše myšlienky, predpoklady, či interpretácie zodpovedajú faktom a či sa neobjavujú extrémne \"čiernobiele\" myšlienky. Dialektickejší pohľad na tieto skutočnosti nám môže pomôcť zmeniť naše emócie. <br><br> Spúšťač emócie môže byť udalosť mimo nás, alebo aj v nás. Emócia môže byť reakciou na našu predošlú emóciu (keď sa hneváme na seba, že máme strach), myšlienku (keď sa cítime previnilo, že niekoho odsudzujeme), pocity zlyhania (keď zabudneme ako sa niekto volá), ale tiež na niečo čo nás potešilo (dobrá známka z matematiky), či sa nám podarilo (osloviť svoju tajnú lásku)."),
        CheckTheFactsQuestion(3, "Čo sú tvoje interpretácie (myšlienky, domnienky) daných faktov?",
            "<h6>Hnev</h6> Fakt: Niekto ťa za niečo kritizuje.<br> Interpretácia: Chce ti hovoriť čo môžeš a nemôžeš robiť!" +
            "<h6>Láska</h6> Fakt: Zistím, že sa so mnou chce vyspať.<br> Interpretácia: Okamžite predpokladám, že ma miluje." +
            "<h6>Odpor</h6> Fakt: Nejaký chlap z ulice sa mi pozerá do okna.<br> Interpretácia: Je to úchyl, sexuálny predátor!" +
            "<h6>Závisť</h6> Fakt: Vidím ako niekto dostáva objatie.<br> Interpretácia: Mám pocit, že ho/ju majú radšej než mňa." +
            "<h6>Šťastie</h6> Fakt: Vidím ráno jasnú oblohu.<br> Interpretácia: Cestou domov určite tiež nebude pršať." +
            "<h6>Žiarlivosť</h6> Fakt: Zbadám milovanú osobu v prítomnosti niekoho iného.<br> Interpretácia: Predpokladám, že ho/ju miluje." +
            "<h6>Smútok</h6> Fakt: Niekto sa rozhodol spraviť si vlastný program bez teba.<br> Interpretácia: To znamená, že ma nemá rád/rada." +
            "<h6>Hanba</h6> Fakt: Dal som si vlastný gól.<br> Interpretácia: Som nemehlo, zúfalec." +
            "<h6>Vina</h6> Fakt: Nechcem sa s niekým podeliť o jedlo.<br> Interpretácia: To znamená, že som sebecký." +
            "<br><br>" +
            "V rámci overovania faktov je vhodné zvážiť všetky možné interpretácie, pozrieť sa na situáciu z rôznych uhlov pohľadu. Interpretácie, ktoré sú menej katastrofické a viac zodpovedajú faktom nám pomáhajú lepšie zvládať naše emócie."
),
        CheckTheFactsQuestion(4, "Očakávaš nejakú hrozbu?", "Bolestné emócie sa často vzťahujú k nejakému konkrétnemu pocitu ohrozenia. Môžeme napríklad očakávať niečo nepríjemné ak sa zúčastníme obávanej udalosti, pričom častokrát si týchto hrozieb ani nie sme vedomí. <br><br> Napriek tomu je veľmi dôležité sa zamyslieť, či predpokladáme, že nám môže niečo hroziť alebo nás niečo ohrozuje. Je to dôležité predovšetkým vtedy, keď sme odhalili všetky fakty a napriek tomu stále pociťujeme príliš intenzívne emócie. To môže znamenať, že naša emócia je reakciou na nejakú hypotetickú hrozbu, ktorej sa možno obávame. <br><br> V tomto kroku pomenujeme hrozbu, vrátane prežívanej emócie. Tu uvádzame niekoľko príkladov hrozieb ktoré môžeme vnímať keď prežívame uvedenú emóciu. <br><br>" +
                "<h6>Hnev</h6> Obava že niekto na mňa útočí. Obava, že sa mi niečo dôležité nepodarí." +
                "<h6>Odpor</h6> Obava zo znečistenia/nakazenia." +
                "<h6>Strach</h6> Obava z ohrozenia zdravia alebo života." +
                "<h6>Smútok</h6> Obava, že niekoho naveky stratíme. Obava, že sa nám nepodarí dosiahnuť naše ciele." +
                "<h6>Hanba</h6> Obava, že nás ľudia odmietnu." +
                "<h6>Vina</h6> Obava, že porušíme naše vlastné hodnoty." +
                "<h6>Žiarlivosť</h6> Obava, že niekto iný sa zmocní pre nás cennej osoby či veci." +
                "<h6>Závisť</h6> Obava, že sa nám nepodarí získať to po čom túžime, pretože iní majú výrazne viac moci, vplyvu, alebo prostriedkov."),
        CheckTheFactsQuestion(5, "Aká katastrofa sa môže stať, ak by aj nastala obávaná udalosť?", "Čo keď sa to naozaj stane? Občas sú skutočnosti naozaj také zlé, ako sa zdajú. Častokrát sme to však my, kto danú situáciu ešte zhorší zdôrazňovaním negatívnych následkov faktov a sústredením sa na najhorší možný výsledok udalosti. Katastrofizovanie naozaj môže zvýšiť naše prežívanie fyzickej aj emočnej bolesti. <br><br> Čeliť úspešne katastrofám môžeme pomocou nasledovných DBT techník:<br><div>Riešenie problémov</div><div>Vysporiadanie sa vopred</div><div>Radikálne prijatie</div>"),
        CheckTheFactsQuestion(6, "Nakoľko tvoja emócia (jej dĺžka trvania a sila) zodpovedá faktom?", "V poslednom kroku overíme, či daná emócia, prípadne intenzita, s akou ju prežívame, zodpovedá faktom. Častokrát totiž nie je problém v samotnej emócií, ako skôr v intenzite jej prežívania.<br><br><h6>Príklady</h6><div>Prepustia ma z práce a reagujem ako človek odsúdený na život v chudobe.</div><div>Niekto ma predbehne v potravinách no ja reagujem akoby fyzicky zaútočil na mňa a moje dieťa.</div>Niekedy sa v noci zobudíme a hlavu máme plnú rôznych znepokojivých myšlienok a katastrofických scenárov. Pritom ráno už nedokážeme pochopiť, ako sme ich mohli brať vážne. Vtedy je lepšie si povedať “toto sú myšlienky, ktorým sa budem venovať až ráno” a odložiť ich na neskôr.")
    )

    val questionIterator = questions.iterator()
}