package palenik.fi.muni.dbtkouc.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.custom_row.view.*
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.model.CaptureEmotionAnswer

class ListAdapter: RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var captureEmotionAnswerList = emptyList<CaptureEmotionAnswer>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = captureEmotionAnswerList[position]
        holder.itemView.whenDidItHappenText.text = currentItem.weekday.toString()
        holder.itemView.partOfDayText.text = currentItem.partOfDay.toString()
        holder.itemView.placeText.text = currentItem.place.toString()
        holder.itemView.whatDidYouFeelAnswerText.text = currentItem.whatDidYouFeelAnswer.toString()
        holder.itemView.whatDidYouDoAnswerText.text = currentItem.whatDidYouDo.toString()

        // add emotion emoticon
        var emoticon = 0 // no drawable by default
        when (holder.itemView.whatDidYouFeelAnswerText.text) {
            "hnev" -> emoticon = R.drawable.angry
            "smútok" -> emoticon = R.drawable.sad
            "úzkosť" -> emoticon = R.drawable.anxiety
            "trému" -> emoticon = R.drawable.stagefright
            "ľútosť" -> emoticon = R.drawable.regret
            "zdesenie" -> emoticon = R.drawable.shock

            "spokojnosť" -> emoticon = R.drawable.satisfaction
            "veselosť" -> emoticon = R.drawable.happiness
            "kľud" -> emoticon = R.drawable.calm
            "úspech" -> emoticon = R.drawable.success
            "bláznivosť" -> emoticon = R.drawable.craziness
            "zamilovanosť" -> emoticon = R.drawable.in_love
        }

        holder.itemView.whatDidYouFeelAnswerText.setCompoundDrawablesWithIntrinsicBounds(emoticon, 0, 0, 0)

        when(currentItem.haveYouToldAboutYourFeelings){
            true -> {holder.itemView.spokedAboutFeelingsCheckedText.isChecked = true}
            false -> {holder.itemView.spokedAboutFeelingsCheckedText.isChecked = false}
        }
    }

    override fun getItemCount(): Int {
        return captureEmotionAnswerList.size
    }

    fun setData(captureEmotionAnswers : List<CaptureEmotionAnswer>){
        this.captureEmotionAnswerList = captureEmotionAnswers
        notifyDataSetChanged()
    }
}