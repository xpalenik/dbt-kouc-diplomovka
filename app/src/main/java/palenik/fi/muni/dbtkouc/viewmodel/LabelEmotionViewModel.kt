package palenik.fi.muni.dbtkouc.viewmodel

import androidx.lifecycle.ViewModel
import palenik.fi.muni.dbtkouc.model.Emotion

class LabelEmotionViewModel : ViewModel() {

    private val emotions = arrayListOf(
        Emotion(
            "Strach",
            "Keď niečo/niekto ohrozuje naše zdravie, alebo život.",
            "emotion_fear"
        ),
        Emotion(
            "Hnev",
            "Keď nám nejaká prekážka bráni v dosiahnutí nášho cieľa, alebo nás niečo/niekto ohrozuje.",
            "emotion_anger"
        ),
        Emotion(
            "Závisť",
            "Keď má niekto majetok, vzťah, situáciu, alebo status, ktorý my nemáme, ale túžime po ňom.",
            "emotion_envy"
        ),
        Emotion(
            "Smútok",
            "Keď niečo/niekoho stratíme.",
            "emotion_sadness"
        ),
        Emotion(
            "Vina",
            "Keď sme vykonali niečo, čo prekračuje normy sociálnej skupiny, ale nie je to tak strašné, aby bezprostredne hrozilo vylúčenie.",
            "emotion_shame"
        )
    )

    val emotionIterator = emotions.iterator()
}