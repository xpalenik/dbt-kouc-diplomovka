package palenik.fi.muni.dbtkouc.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import palenik.fi.muni.dbtkouc.model.CaptureEmotionAnswer
import palenik.fi.muni.dbtkouc.model.CaptureEmotionAnswerDatabase
import palenik.fi.muni.dbtkouc.model.CaptureEmotionAnswerRepository

@InternalCoroutinesApi
class CaptureEmotionAnswerViewModel(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<CaptureEmotionAnswer>>
    private val repository: CaptureEmotionAnswerRepository

    init {
        val captureEmotionAnswerDao = CaptureEmotionAnswerDatabase.getDatabase(application).captureEmotionAnswerDao()
        repository = CaptureEmotionAnswerRepository(captureEmotionAnswerDao)
        readAllData = repository.readAllData
    }

    fun addCaptureEmotionAnswer(captureEmotionAnswer: CaptureEmotionAnswer){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addCaptureEmotionAnswer(captureEmotionAnswer)
        }
    }
}