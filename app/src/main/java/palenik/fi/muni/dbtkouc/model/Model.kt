package palenik.fi.muni.dbtkouc.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "myths")
data class Myth(
    val myth: String?,
    val evaluation: String?
) {
    @PrimaryKey(autoGenerate = true)
    var uuid: Int = 0
}

@Entity(tableName = "quiz_tasks")
data class QuizTask(
    val task: String?,
    val answer: Boolean?,
    val explanation: String?
) {
    @PrimaryKey(autoGenerate = true)
    var uuid: Int = 0
}

@Entity(tableName = "check_the_facts_answers")
data class CheckTheFactsAnswer(
    val questionNumber: Int?,
    val answer: String?,
    val date: Date
) {
    @PrimaryKey(autoGenerate = true)
    var uuid: Int = 0
}

data class CheckTheFactsQuestion(
    val questionNumber: Int?,
    val question:String?,
    val explanation: String?
)

data class Emotion(
    val label: String?,
    val explanation:String?,
    val imageNameWithoutExtension: String
)

data class CaptureEmotionTimeAndPlace(
    val weekDay: String?,
    val timeOfDay: String?,
    val place: String?
)

@Entity(tableName = "capture_emotion_answers")
data class CaptureEmotionAnswer(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    var weekday : String?,
    var place : String?,
    var partOfDay : String?,
    var whatDidYouFeelAnswer : String?,
    var haveYouToldAboutYourFeelings : Boolean?,
    var whatDidYouDo : String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(weekday)
        parcel.writeString(place)
        parcel.writeString(partOfDay)
        parcel.writeString(whatDidYouFeelAnswer)
        parcel.writeValue(haveYouToldAboutYourFeelings)
        parcel.writeString(whatDidYouDo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CaptureEmotionAnswer> {
        override fun createFromParcel(parcel: Parcel): CaptureEmotionAnswer {
            return CaptureEmotionAnswer(parcel)
        }

        override fun newArray(size: Int): Array<CaptureEmotionAnswer?> {
            return arrayOfNulls(size)
        }
    }
}