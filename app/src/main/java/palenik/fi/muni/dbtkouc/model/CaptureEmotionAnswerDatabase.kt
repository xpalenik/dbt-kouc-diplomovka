package palenik.fi.muni.dbtkouc.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized

@Database(entities = [CaptureEmotionAnswer::class], version = 1, exportSchema = false)
abstract class CaptureEmotionAnswerDatabase: RoomDatabase() {

    abstract  fun captureEmotionAnswerDao(): CaptureEmotionAnswerDao

    companion object{
        @Volatile
        private var INSTANCE: CaptureEmotionAnswerDatabase? = null

        @InternalCoroutinesApi
        fun getDatabase(context: Context): CaptureEmotionAnswerDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CaptureEmotionAnswerDatabase::class.java,
                    "capture_emotion_answers"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}