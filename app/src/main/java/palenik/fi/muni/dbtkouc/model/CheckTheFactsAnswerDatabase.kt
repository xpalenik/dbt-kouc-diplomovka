package palenik.fi.muni.dbtkouc.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import palenik.fi.muni.dbtkouc.util.Converters

@Database(entities = [CheckTheFactsAnswer::class], version = 1)
@TypeConverters(Converters::class)
abstract class CheckTheFactsAnswerDatabase : RoomDatabase() {

    abstract fun checkTheFactsAnswerDao(): CheckTheFactsAnswerDao

    companion object {
        @Volatile
        private var instance: CheckTheFactsAnswerDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            CheckTheFactsAnswerDatabase::class.java,
            "checkthefactsanswerdatabase"
        ).build()
    }

}