package palenik.fi.muni.dbtkouc.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Myth::class], version = 1)
abstract class MythDatabase : RoomDatabase() {
    abstract fun mythDao(): MythDao

    companion object {
        @Volatile
        private var instance: MythDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            MythDatabase::class.java,
            "mythdatabase"
        )
            .createFromAsset("mythdatabase")
            .build()
    }
}