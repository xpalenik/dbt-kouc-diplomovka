package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_capture_emotion2.*
import palenik.fi.muni.dbtkouc.R

class CaptureEmotionFragment2 : Fragment() {

    private val args: CaptureEmotionFragment2Args by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_capture_emotion2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun nextFragment(){
            val action = CaptureEmotionFragment2Directions.actionCaptureEmotionFragment3(args.capturedEmotionAnswer)
            Navigation.findNavController(view).navigate(action)
        }

        whatDidYouFeelAnswer1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                args.capturedEmotionAnswer.whatDidYouFeelAnswer = parent?.getItemAtPosition(position).toString()
                if (position != 0) nextFragment()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        whatDidYouFeelAnswer2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                args.capturedEmotionAnswer.whatDidYouFeelAnswer = parent?.getItemAtPosition(position).toString()
                if (position != 0) nextFragment()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

    }
}