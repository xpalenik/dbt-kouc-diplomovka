package palenik.fi.muni.dbtkouc.model

import androidx.lifecycle.LiveData

class CaptureEmotionAnswerRepository(private val captureEmotionAnswerDao: CaptureEmotionAnswerDao) {

    val readAllData: LiveData<List<CaptureEmotionAnswer>> = captureEmotionAnswerDao.readAllData()

    suspend fun addCaptureEmotionAnswer(captureEmotionAnswer: CaptureEmotionAnswer){
        captureEmotionAnswerDao.addCaptureEmotionAnswer(captureEmotionAnswer)
    }
}