package palenik.fi.muni.dbtkouc.view

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.setPadding
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_quiz.*
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.databinding.FragmentQuizBinding
import palenik.fi.muni.dbtkouc.viewmodel.QuizViewModel

class QuizFragment : Fragment() {

    private lateinit var viewModel: QuizViewModel
    private lateinit var dataBinding: FragmentQuizBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quiz, container, false)
        return dataBinding.root
    }

    private fun showExplanation() {
        lottie.speed = 1.3F
        lottie.repeatCount = 0
        lottie.alpha = 0f
        lottie.visibility = View.VISIBLE
        lottie.animate()
            .alpha(1f).duration = 600

        Handler(Looper.getMainLooper()).postDelayed({
                hideButtons()

                speech_bubble.visibility = View.VISIBLE
                speech_bubble.speed = 0.8F
                speech_bubble.playAnimation()

                quizTaskExplanation.alpha = 0f
                quizTaskExplanation.visibility = View.VISIBLE
                quizTaskExplanation.animate()
                    .alpha(1f).duration = 600

                buttonNextQuizTask.alpha = 0f
                buttonNextQuizTask.visibility = View.VISIBLE
                buttonNextQuizTask.animate()
                    .alpha(1f).duration = 600
        }, 1500)
    }

    private fun disableButton(button: Button) {
        button.isEnabled = false
        button.isClickable = false
    }

    private fun enableButton(button: Button) {
        button.isEnabled = true
        button.isClickable = true
    }

    private fun setOnclickListenersForYesNoButtons(answer: Boolean) {

        val red = Color.rgb(255, 102, 102)
        val green = Color.rgb(51, 255, 153)

        buttonYes.setOnClickListener {
            if (answer) {
                buttonYes.setBackgroundColor(green)
                disableButton(buttonNo)
                lottie.setAnimation(R.raw.quiz_good_answer)
                lottie.playAnimation()
            } else {
                buttonYes.setBackgroundColor(red)
                disableButton(buttonNo)
                lottie.setAnimation(R.raw.quiz_bad_answer)
                lottie.playAnimation()
            }
            showExplanation()
        }

        buttonNo.setOnClickListener {
            if (!answer) {
                buttonNo.setBackgroundColor(green)
                disableButton(buttonYes)
                lottie.setAnimation(R.raw.quiz_good_answer)
                lottie.playAnimation()
            } else {
                buttonNo.setBackgroundColor(red)
                disableButton(buttonYes)
                lottie.setAnimation(R.raw.quiz_bad_answer)
                lottie.playAnimation()
            }
            showExplanation()
        }

    }

    private fun hideButtons() {
        buttonYes.visibility = View.GONE
        buttonNo.visibility = View.GONE
    }

    private fun showButtons() {
        buttonYes.visibility = View.VISIBLE
        buttonNo.visibility = View.VISIBLE
    }

    // source: https://stackoverflow.com/a/4275969
    private fun convertDpToPx(dp: Int): Int{
        val scale = resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this)[QuizViewModel::class.java]
        viewModel.fetchFromDatabase()
        setOnclickListenersForYesNoButtons(false)

        buttonNextQuizTask.setOnClickListener {
            buttonNextQuizTask.visibility = View.GONE
            quizTaskExplanation.visibility = View.GONE
            speech_bubble.progress = 0F
            lottie.progress = 0F
            speech_bubble.visibility = View.GONE
            lottie.visibility = View.GONE
            showButtons()

            buttonYes.background = Button(context).background
            buttonYes.setPadding(convertDpToPx(16))
            enableButton(buttonYes)

            buttonNo.background = Button(context).background
            buttonNo.setPadding(convertDpToPx(16))
            enableButton(buttonNo)

            viewModel.quizTasksIterator.let {
                if (it.hasNext()) {
                    val quizTask = it.next()
                    quizTaskText.text = quizTask.task
                    quizTaskExplanation.text = quizTask.explanation

                    if (quizTask.answer != null) {
                        setOnclickListenersForYesNoButtons(quizTask.answer)
                    }
                } else {
                    val action = QuizFragmentDirections.actionMenuFragment()
                    Navigation.findNavController(view).navigate(action)
                }
            }
        }
    }

}