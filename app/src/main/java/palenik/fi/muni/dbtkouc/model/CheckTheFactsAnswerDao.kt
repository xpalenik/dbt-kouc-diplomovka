package palenik.fi.muni.dbtkouc.model

import androidx.room.Dao
import androidx.room.Query

@Dao
interface CheckTheFactsAnswerDao {

    @Query("SELECT * FROM check_the_facts_answers WHERE uuid = :answerId")
    suspend fun getAnswer(answerId: Int): CheckTheFactsAnswer

    @Query("DELETE FROM check_the_facts_answers")
    suspend fun deleteAllAnswers()

    @Query("DELETE FROM check_the_facts_answers WHERE uuid = :answerId")
    suspend fun deleteAnswer(answerId: Int)

}