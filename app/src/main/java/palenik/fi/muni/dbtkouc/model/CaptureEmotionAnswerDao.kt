package palenik.fi.muni.dbtkouc.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CaptureEmotionAnswerDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addCaptureEmotionAnswer(captureEmotionAnswer: CaptureEmotionAnswer)

    @Query("SELECT * FROM capture_emotion_answers ORDER BY id ASC")
    fun readAllData(): LiveData<List<CaptureEmotionAnswer>>

}