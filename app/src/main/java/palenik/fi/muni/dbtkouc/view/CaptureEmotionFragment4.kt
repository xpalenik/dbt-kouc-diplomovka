package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_capture_emotion1.buttonNext
import kotlinx.android.synthetic.main.fragment_capture_emotion4.*
import kotlinx.coroutines.InternalCoroutinesApi
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.viewmodel.CaptureEmotionAnswerViewModel

class CaptureEmotionFragment4 : Fragment() {

    @InternalCoroutinesApi
    private lateinit var captureEmotionAnswerViewModel: CaptureEmotionAnswerViewModel

    private val args: CaptureEmotionFragment4Args by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_capture_emotion4, container, false)
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        whatDidYouDoAnswer.doAfterTextChanged {
            args.capturedEmotionAnswer.whatDidYouDo = whatDidYouDoAnswer.text.toString()
        }

        captureEmotionAnswerViewModel = ViewModelProvider(this).get(CaptureEmotionAnswerViewModel::class.java)

        buttonNext.setOnClickListener {
            insertDataToDatabase()
            val action = CaptureEmotionFragment4Directions.actionListCapturedEmotionsFragment()
            Navigation.findNavController(view).navigate(action)
        }
    }

    @InternalCoroutinesApi
    private fun insertDataToDatabase() {
        captureEmotionAnswerViewModel.addCaptureEmotionAnswer(args.capturedEmotionAnswer)
    }
}