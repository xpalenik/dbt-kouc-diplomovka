package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_label_emotion.*
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.databinding.FragmentLabelEmotionBinding
import palenik.fi.muni.dbtkouc.viewmodel.LabelEmotionViewModel


class LabelEmotionFragment : Fragment() {

    private lateinit var viewModel: LabelEmotionViewModel
    private lateinit var dataBinding: FragmentLabelEmotionBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_label_emotion, container, false)
        return dataBinding.root
    }

    private fun getImageResource(imageNameWithoutExtension: String?): Int {
        when (imageNameWithoutExtension) {
            "emotion_anger" -> return R.drawable.emotion_anger
            "emotion_shame" -> return R.drawable.emotion_shame
            "emotion_envy" -> return R.drawable.emotion_envy
            "emotion_fear" -> return R.drawable.emotion_fear
            "emotion_sadness" -> return R.drawable.emotion_sadness
        }
        return 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this)[LabelEmotionViewModel::class.java]
        val iter = viewModel.emotionIterator

        if (iter.hasNext()) {
            val next = iter.next()

            imageEmotion.setImageResource(getImageResource(next.imageNameWithoutExtension))
            dataBinding.emotion = next
        }

        buttonReveal.setOnClickListener {
            val fadeInAnimation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.label_emotion_fade_in_anim)

            buttonReveal.visibility = View.INVISIBLE

            emotionLabel.startAnimation(fadeInAnimation)
            emotionLabel.visibility = View.VISIBLE

            imageEmotion.startAnimation(fadeInAnimation)
            imageEmotion.visibility = View.VISIBLE
        }

        buttonNextEmotion.setOnClickListener {
            if (iter.hasNext()) {
                val next = iter.next()

                buttonReveal.visibility = View.VISIBLE
                emotionLabel.visibility = View.INVISIBLE
                imageEmotion.visibility = View.INVISIBLE

                imageEmotion.setImageResource(getImageResource(next.imageNameWithoutExtension))
                dataBinding.emotion = next
            } else {
                val action = LabelEmotionFragmentDirections.actionMenuFragment()
                Navigation.findNavController(view).navigate(action)
            }
        }
    }

}