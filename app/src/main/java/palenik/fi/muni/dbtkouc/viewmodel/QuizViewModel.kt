package palenik.fi.muni.dbtkouc.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import palenik.fi.muni.dbtkouc.model.QuizTask
import palenik.fi.muni.dbtkouc.model.QuizDatabase

class QuizViewModel(application: Application) : BaseViewModel(application) {

    private val quizTasks = MutableLiveData<List<QuizTask>>()
    lateinit var quizTasksIterator: Iterator<QuizTask>

    fun fetchFromDatabase() {
        launch {
            val quizTaskList = QuizDatabase(getApplication()).quizDao().getAllQuizTasks()
            quizTasks.value = quizTaskList
            quizTasks.value?.let {
                quizTasksIterator = it.iterator()
            }
        }
    }
}