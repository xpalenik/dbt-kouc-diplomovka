package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_check_the_facts.*
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.databinding.FragmentCheckTheFactsBinding
import palenik.fi.muni.dbtkouc.viewmodel.CheckTheFactsViewModel

class CheckTheFactsFragment : Fragment() {

    private lateinit var viewModel: CheckTheFactsViewModel
    private lateinit var dataBinding: FragmentCheckTheFactsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_check_the_facts, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this)[CheckTheFactsViewModel::class.java]
        val iter = viewModel.questionIterator

        var next = iter.next()
        questionNumber.text = next.questionNumber.toString().plus("/6")
        questionText.text = next.question
        explanation.text = HtmlCompat.fromHtml(next.explanation.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)

        buttonNextFactsQuestion.setOnClickListener {
                if (iter.hasNext()) {
                    next = iter.next()
                    questionNumber.text = next.questionNumber.toString().plus("/6")
                    questionText.text = next.question
                    explanation.text = HtmlCompat.fromHtml(next.explanation.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
                } else {
                    val action = CheckTheFactsFragmentDirections.actionMenuFragment()
                    Navigation.findNavController(view).navigate(action)
                }
        }
    }

}