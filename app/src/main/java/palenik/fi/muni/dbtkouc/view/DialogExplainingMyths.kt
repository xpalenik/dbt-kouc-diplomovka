package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.explain_myths.*
import palenik.fi.muni.dbtkouc.R

class DialogExplainingMyths : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.explain_myths, container, false)
    }

    override fun onStart() {
        super.onStart()

        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.51).toInt()
        dialog!!.window?.setLayout(width, height)
        dialog!!.window?.setWindowAnimations(R.style.animations)

        goToMythsButton.setOnClickListener {
            val action = DialogExplainingMythsDirections.actionMythFragment()
            parentFragment?.findNavController()?.navigate(action)
            dialog!!.hide()
        }

    }

}