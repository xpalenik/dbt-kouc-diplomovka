package palenik.fi.muni.dbtkouc.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import palenik.fi.muni.dbtkouc.model.Myth
import palenik.fi.muni.dbtkouc.model.MythDatabase


class MythViewModel(application: Application) : BaseViewModel(application) {

    private val myths = MutableLiveData<List<Myth>>()
    lateinit var mythsIterator: Iterator<Myth>

    fun fetchFromDatabase() {
        launch {
            val mythList = MythDatabase(getApplication()).mythDao().getAllMyths()
            myths.value = mythList
            myths.value?.let {
                mythsIterator = it.iterator()
            }
        }
    }
}