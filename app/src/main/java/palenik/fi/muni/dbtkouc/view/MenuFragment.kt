package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_menu.*
import palenik.fi.muni.dbtkouc.R


class MenuFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonMyths.setOnClickListener {
            DialogExplainingMyths().show(parentFragmentManager, "DialogExplainingExercise")
        }

        buttonQuiz.setOnClickListener {
            val action = MenuFragmentDirections.actionQuizFragment()
            Navigation.findNavController(it).navigate(action)
        }

        buttonFacts.setOnClickListener {
            val action = MenuFragmentDirections.actionFactsFragment()
            Navigation.findNavController(it).navigate(action)
        }

        buttonLabelEmotion.setOnClickListener {
            val action = MenuFragmentDirections.actionEmotionFragment()
            Navigation.findNavController(it).navigate(action)
        }

        buttonCaptureEmotion.setOnClickListener {
            DialogExplainingCaptureEmotion().show(parentFragmentManager, "DialogExplainingExercise")
        }

        buttonQuestionary.setOnClickListener {
            DialogExplainingTestNeedsInRelationship().show(parentFragmentManager, "DialogExplainingExercise")
        }

        buttonEmotionDiary.setOnClickListener {
            val action = MenuFragmentDirections.actionListCapturedEmotionsFragment()
            Navigation.findNavController(it).navigate(action)
        }

    }
}