package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_capture_emotion3.*
import palenik.fi.muni.dbtkouc.R

class CaptureEmotionFragment3 : Fragment() {

    private val args: CaptureEmotionFragment3Args by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_capture_emotion3, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun nextFragment(){
            val action = CaptureEmotionFragment3Directions.actionCaptureEmotionFragment4(args.capturedEmotionAnswer)
            Navigation.findNavController(view).navigate(action)
        }

        buttonYesInCaptureEmotion.setOnClickListener {
            args.capturedEmotionAnswer.haveYouToldAboutYourFeelings = true
            nextFragment()
        }

        buttonNoInCaptureEmotion.setOnClickListener {
            args.capturedEmotionAnswer.haveYouToldAboutYourFeelings = false
            nextFragment()
        }

    }
}