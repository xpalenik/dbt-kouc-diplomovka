package palenik.fi.muni.dbtkouc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_capture_emotion1.*
import palenik.fi.muni.dbtkouc.R
import palenik.fi.muni.dbtkouc.model.CaptureEmotionAnswer

class CaptureEmotionFragment1 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_capture_emotion1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val captureEmotionAnswer = CaptureEmotionAnswer(0,"","","","",false,"")

        weekday.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                captureEmotionAnswer.weekday = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                captureEmotionAnswer.weekday = resources.getStringArray(R.array.weekdays).first()
            }
        }

        partOfDay.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                captureEmotionAnswer.partOfDay = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                captureEmotionAnswer.partOfDay = resources.getStringArray(R.array.partOfDay).first()
            }
        }

        place.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                captureEmotionAnswer.place = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                captureEmotionAnswer.place = resources.getStringArray(R.array.places).first()
            }
        }

        buttonNext.setOnClickListener {
            val action = CaptureEmotionFragment1Directions.actionCaptureEmotionFragment2(captureEmotionAnswer)
            Navigation.findNavController(view).navigate(action)
        }
    }

}