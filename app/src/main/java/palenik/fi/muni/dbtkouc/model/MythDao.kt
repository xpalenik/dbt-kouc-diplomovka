package palenik.fi.muni.dbtkouc.model

import androidx.room.Dao
import androidx.room.Query

@Dao
interface MythDao {

    @Query("SELECT * FROM myths")
    suspend fun getAllMyths(): List<Myth>

    @Query("SELECT * FROM myths WHERE uuid = :mythId")
    suspend fun getMyth(mythId: Int): Myth

    @Query("DELETE FROM myths")
    suspend fun deleteAllMyths()

    @Query("DELETE FROM myths WHERE uuid = :mythId")
    suspend fun deleteMyth(mythId: Int)

    @Query("SELECT COUNT(myth) FROM myths")
    suspend fun getRowCount(): Int
}